Um die Data-Conversion mit Slicer für die Segmentierungen durchzuführen, muss in Slicer die Extension JupyterKernel installiert werden, 
und dieser Kernel dann über das Termin in Jupyter installiert werden. 
Der Befehl dafür ist in Slicer abrufbar sobald die Extension installiert und aufgerufen wurde.
